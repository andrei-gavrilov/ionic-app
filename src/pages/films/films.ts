import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database';
import { FilmDetailsPage } from '../film-details/film-details';

/**
 * Generated class for the FilmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-films',
  templateUrl: 'films.html',
})
export class FilmsPage {
    films = [];
    public category: any = {};

    constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, public navParams: NavParams) {

        this.category=navParams.get('category');



        this.databaseprovider.getDatabaseState().subscribe(rdy => {
            if (rdy) {
              this.loadFilms();
            }
        })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad FilmsPage');
    }
    loadFilms() {
        this.databaseprovider.getAllFilmsByCategoryId(this.category.id).then(data => {
            this.films = data;
        })
    }
    public filmTapped(event, film){
        this.navCtrl.push(FilmDetailsPage,{
            film: film
        });
    }

}
