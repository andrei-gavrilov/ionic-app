//import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { DatabaseProvider } from './../../providers/database/database';
import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';


/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { FilmsPage } from '../films/films';

@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html',
})
export class CategoriesPage {
    categories = [];
    constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, private platform: Platform) {
        this.databaseprovider.getDatabaseState().subscribe(rdy => {
          if (rdy) {
            this.loadCategories();
          }
        })

    }
    loadCategories() {
        this.databaseprovider.getAllCategories().then(data => {
            this.categories = data;
        })
    }
    public categoryTapped(event, category){
        this.navCtrl.push(FilmsPage,{
            category: category
        });
    }
}
