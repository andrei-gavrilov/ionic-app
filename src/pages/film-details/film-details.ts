import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FilmDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-film-details',
    templateUrl: 'film-details.html',
})
export class FilmDetailsPage {
    public film: any = {};
    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.film=navParams.get('film');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad FilmDetailsPage');
    }

}
