CREATE TABLE IF NOT EXISTS category (
                        id INTEGER PRIMARY KEY, 
                        name TEXT
                    );
CREATE TABLE IF NOT EXISTS film ( 
                        id INTEGER PRIMARY KEY, 
                        name TEXT, 
                        description TEXT, 
                        release_year TEXT
                    );
CREATE TABLE IF NOT EXISTS category_films (
                        category_id INTEGER,
                        film_id INTEGER,
                        FOREIGN KEY(category_id) REFERENCES category(id),
                        FOREIGN KEY(film_id) REFERENCES film(id)
                    );

INSERT INTO category(id, name) VALUES (1, 'Action');
INSERT INTO category(id, name) VALUES (2, 'Drama');
INSERT INTO category(id, name) VALUES (3, 'Comedy');

INSERT INTO film(id, name, description, release_year) VALUES (
                        1, 
                        'Titanic', 
                        'A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the luxurious, ill-fated R.M.S. Titanic.',
                        '1997'
                    );
INSERT INTO film(id, name, description, release_year) VALUES (
                        2, 
                        'Warcraft: The Beginning', 
                        'As an Orc horde invades the planet Azeroth using a magic portal, a few human heroes and dissenting Orcs must attempt to stop the true evil behind this war.',
                        '2016'
                    );

INSERT INTO category_films(category_id, film_id) VALUES (1, 1);
INSERT INTO category_films(category_id, film_id) VALUES (2, 1);
INSERT INTO category_films(category_id, film_id) VALUES (3, 1);
INSERT INTO category_films(category_id, film_id) VALUES (1, 2);
INSERT INTO category_films(category_id, film_id) VALUES (2, 2);