import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

    database: SQLiteObject;
    private databaseReady: BehaviorSubject<boolean>;
   
    constructor(public sqlitePorter: SQLitePorter, private storage: Storage, private sqlite: SQLite, private platform: Platform, private http: Http) {
      this.databaseReady = new BehaviorSubject(false);
      this.platform.ready().then(() => {
        this.sqlite.create({
          name: 'developers.db',
          location: 'default'
        })
          .then((db: SQLiteObject) => {
            this.database = db;
            this.storage.get('database_filled').then(val => {
              if (val) {
                this.databaseReady.next(true);
              } else {
                this.fillDatabase();
              }
            });
          });
      });
    }
    fillDatabase() {
        this.http.get('assets/dummyDump.sql')
          .map(res => res.text())
          .subscribe(sql => {
            this.sqlitePorter.importSqlToDb(this.database, sql)
              .then(data => {
                this.databaseReady.next(true);
                this.storage.set('database_filled', true);
              })
              .catch(e => console.error(e));
          });
      }

      getAllCategories() {
        return this.database.executeSql("SELECT * FROM category", []).then((data) => {
          let categories = [];
          if (data.rows.length > 0) {
            for (var i = 0; i < data.rows.length; i++) {
                categories.push({ id: data.rows.item(i).id, name: data.rows.item(i).name});
            }
          }
          return categories;
        }, err => {
          console.log('Error: ', err);
          return [];
        });
      }

      getAllFilmsByCategoryId(id) {
        return this.database.executeSql("SELECT * FROM film INNER JOIN category_films ON film.id=category_films.film_id WHERE category_id=?;", [id]).then((data) => {
          let films = [];
          if (data.rows.length > 0) {
            for (var i = 0; i < data.rows.length; i++) {
                films.push({ id: data.rows.item(i).id, name: data.rows.item(i).name, description: data.rows.item(i).description, release_year: data.rows.item(i).release_year});
            }
          }
          return films;
        }, err => {
          console.log('Error: ', err);
          return [];
        });
      }
     
      getDatabaseState() {
        return this.databaseReady.asObservable();
      }
     
    }

